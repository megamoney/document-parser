//
// Created by ialex on 22.09.2018.
//

#ifndef PERSPECTIVEWARP_ROMANIANID_H
#define PERSPECTIVEWARP_ROMANIANID_H


#include <vector>
#include <string>

class RomanianID {
public:
    std::vector<std::string> mrz_fields;
    std::vector<std::string> other_fields;
};


#endif //PERSPECTIVEWARP_ROMANIANID_H

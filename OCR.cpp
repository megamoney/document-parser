//
// Created by ialex on 22.09.2018.
//

#include "OCR.h"

tesseract::TessBaseAPI *api;

void OCR::test_tess(cv::Mat &image, std::vector<cv::Rect> fields, RomanianID &id) {
    if (first) {
        init();
    }

    //api->SetImage(image.data, image.cols, image.rows, 4, 4*image.cols);
    for (const auto &field : fields) {
        cv::Mat current = image(field);

        api->SetImage((uchar*)current.data, current.size().width, current.size().height, current.channels(), current.step1());
        api->Recognize(nullptr);

        // Get OCR result
        auto outText = api->GetUTF8Text();
        //printf("%s\n", outText);

        std::string str = std::string(outText);
        parse(str, id);

        delete[] outText;
    }

    // Destroy used object and release memory
    api->End();
}

void OCR::init() {
    if (!first) {
        return;
    }

    api = new tesseract::TessBaseAPI();
    // Initialize tesseract-ocr with English, without specifying tessdata path
    if (api->Init("/home/ialex/CLionProjects/carditionnative/tessdata/", "ron")) {
        //fprintf(stderr, "Could not initialize tesseract.\n");
        exit(1);
    }

    first = false;
}

OCR::~OCR() {

}

void OCR::parse(std::string &str, RomanianID &id) {
    //std::replace(s.begin(), s.end(), ' ', ''); // replace all 'x' to 'y'
    if (str.find('<') != std::string::npos && str.length() >= 36) {
        id.mrz_fields.push_back(str);
    } else {
        id.other_fields.push_back(str);
    }
}

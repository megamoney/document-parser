//
// Created by ialex on 22.09.2018.
//

#ifndef PERSPECTIVEWARP_UTILS_H
#define PERSPECTIVEWARP_UTILS_H

#include <opencv2/imgproc.hpp>
#include <opencv2/core/mat.hpp>

class Utils {
public:
    void scale(cv::Mat&);
private:
    void resize_to_width(cv::Mat&,int);
    void resize_to_height(cv::Mat&,int);
};


#endif //PERSPECTIVEWARP_UTILS_H

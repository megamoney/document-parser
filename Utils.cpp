//
// Created by ialex on 22.09.2018.
//

#include "Utils.h"

void Utils::resize_to_width(cv::Mat &obj, int desired_width) {
    float ratio = (float) desired_width / obj.cols;
    int calculated_height = (int) (obj.rows * ratio);

    cv::resize(obj, obj, cv::Size(desired_width, calculated_height));
}

void Utils::resize_to_height(cv::Mat &obj, int desired_height) {
    float ratio = (float) desired_height / obj.rows;
    int calculated_width = (int) (obj.cols * ratio);

    cv::resize(obj, obj, cv::Size(calculated_width, desired_height));
}

void Utils::scale(cv::Mat &obj) {
    if (obj.cols > obj.rows) { //landscape -> scale to 1600 width
        resize_to_width(obj, 1200);
    } else {
        resize_to_height(obj, 1600);
    }
}

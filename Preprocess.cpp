//
// Created by ialex on 22.09.2018.
//

#include "Preprocess.h"

void Preprocess::deskew(cv::Mat &flat_object, cv::Mat &res) {
    cv::Mat flat_object_copy;
    flat_object.copyTo(flat_object_copy);

    // Apply Gaussian blur to get rid off some noise
    cv::GaussianBlur(flat_object_copy, flat_object_copy, cv::Size(33, 33), 0, 0);

    cv::Mat flat_object_hsv;
    cv::cvtColor(flat_object_copy, flat_object_hsv, CV_BGR2HSV);

    std::vector<cv::Mat> channels;
    cv::split(flat_object_hsv, channels);

    cv::Mat hue = channels[0];
    cv::Mat saturation = channels[1];
    cv::Mat value = channels[2];

    //cv::imshow("hue", hue);
    //cv::imshow("sat", saturation);
    //cv::imshow("val", value);

    //threshold to find the contour
    cv::Mat thresholded;
    cv::threshold(saturation, thresholded, 0, 255, CV_THRESH_BINARY_INV | CV_THRESH_OTSU);

    //morphological operations
    cv::Mat thresholded_open;
    cv::Mat thresholded_close;

    cv::Mat structuringElement = cv::getStructuringElement(CV_SHAPE_RECT, cv::Size(7, 7));

    cv::morphologyEx(thresholded, thresholded_open, CV_MOP_OPEN, structuringElement);
    cv::morphologyEx(thresholded_open, thresholded_close, CV_MOP_CLOSE, structuringElement);

    //cv::imshow("thresholded_open", thresholded_open);
    //cv::imshow("thresholded_close", thresholded_close);

    //find edges
    cv::Mat thresholded_edge;
    cv::Canny(thresholded_close, thresholded_edge, 15, 150);
    //cv::imshow("thresholded_edge", thresholded_edge);

    // find all contours in photo
    std::vector<std::vector<cv::Point> > contours;
    std::vector<std::vector<cv::Point> > possible_contours;
    cv::findContours(thresholded_edge, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    for (auto &contour : contours) { // for each contour
        double peri = cv::arcLength(contour, true);
        std::vector<cv::Point> approx;

        cv::approxPolyDP(contour, approx, 0.02 * peri, true);

        if (approx.size() == 4) {
            //std::cout << "Am gasit un contur\n";
            possible_contours.push_back(approx);
        }
    }

    if (possible_contours.size() > 1) {
        std::sort(possible_contours.begin(), possible_contours.end(),
                  [](const std::vector<cv::Point> &c1, const std::vector<cv::Point> &c2) {
                      return contourArea(c1, false) > contourArea(c2, false);
                  });
    }

    if (possible_contours.empty()) {
        //std::cout << "No contours found...trying it on the whole image\n";
        res = flat_object;
        return;
    }

    auto remove = std::remove_if(possible_contours.begin(), possible_contours.end(), [flat_object](std::vector<cv::Point> points) {
        for (auto point : points) {
            if (point.x <= 5 || point.x >= flat_object.cols - 5 ) {
                return true;
            }
            if (point.y <= 5 || point.y >= flat_object.rows - 5 ) {
                return true;
            }
        }
        return false;
    });

    possible_contours.erase(remove, possible_contours.end());

    cv::drawContours(flat_object, possible_contours, -1, cv::Scalar(0, 255, 0), 3);

    four_point_warp(possible_contours[0], flat_object, res);
}

/**
 * Top point has the smallest sum of x and y
 * @param contour
 * @param top
 * @return
 */
cv::Point argmin(std::vector<cv::Point> &contour, bool left) {
    cv::Point ret;

    if (left) {
        int sum = INT_MAX;
        for (auto &point : contour) {
            if (point.x + point.y < sum) {
                sum = point.x + point.y;
                ret = point;
            }
        }
    } else {
        int sum = INT_MAX;
        for (auto &point : contour) {
            if (point.x - point.y < sum) {
                sum = point.x - point.y;
                ret = point;
            }
        }
    }
    return ret;
}

cv::Point argmax(std::vector<cv::Point> &contour, bool right) {
    cv::Point ret;

    if (right) {
        int sum = INT_MIN;
        for (auto &point : contour) {
            if (point.x + point.y > sum) {
                sum = point.x + point.y;
                ret = point;
            }
        }
    } else {
        int sum = INT_MIN;
        for (auto &point : contour) {
            if (point.x - point.y > sum) {
                sum = point.x - point.y;
                ret = point;
            }
        }
    }
    return ret;
}

void Preprocess::four_point_warp(std::vector<cv::Point> &contour, cv::Mat &orig, cv::Mat &warp) {
    cv::Point tl = argmin(contour, true);
    cv::Point bl = argmin(contour, false);
    cv::Point br = argmax(contour, true);
    cv::Point tr = argmax(contour, false);

    // now that we have our rectangle of points, let's compute
    // the width of our new image
    auto widthA = (float) sqrt((br.x - bl.x) * (br.x - bl.x) + (br.y - bl.y) * (br.y - bl.y));
    auto widthB = (float) sqrt((tr.x - tl.x) * (tr.x - tl.x) + (tr.y - tl.y) * (tr.y - tl.y));

    // ...and now for the height of our new image
    auto heightA = (float) sqrt((tr.x - br.x) * (tr.x - br.x) + (tr.y - br.y) * (tr.y - br.y));
    auto heightB = (float) sqrt((tl.x - bl.x) * (tl.x - bl.x) + (tl.y - bl.y) * (tl.y - bl.y));

    float maxWidth = (widthA > widthB) ? widthA : widthB;
    float maxHeight = (heightA > heightB) ? heightA : heightB;

    cv::Point2f src_points[4];
    src_points[0] = tl;
    src_points[1] = tr;
    src_points[2] = bl;
    src_points[3] = br;

    cv::Point2f dst_points[4];
    dst_points[0] = cv::Point2f(0, 0);
    dst_points[1] = cv::Point2f(maxWidth, 0);
    dst_points[2] = cv::Point2f(0, maxHeight);
    dst_points[3] = cv::Point2f(maxWidth, maxHeight);

    cv::Mat transform = cv::getPerspectiveTransform(src_points, dst_points);
    cv::warpPerspective(orig, warp, transform, cv::Size(maxWidth, maxHeight));

    //cv::imshow("result", warp);
}

std::vector<cv::Rect> Preprocess::identify_fields(cv::Mat &rgb) {
    cv::Mat gray;
    cvtColor(rgb, gray, CV_BGR2GRAY);

    // Apply Gaussian blur to get rid off some noise
    //cv::GaussianBlur(gray, gray, cv::Size(25, 25), 0, 0);

    cv::Mat grad;
    cv::Mat morphKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3));
    morphologyEx(gray, grad, cv::MORPH_GRADIENT, morphKernel);

    cv::Mat bw;
    threshold(grad, bw, 0.0, 255.0, CV_THRESH_BINARY | CV_THRESH_OTSU);
    //cv::imshow("bw", bw);

    // Remove any vertical lines so it won't mess up text detection
    // Specify size on horizontal axis
    cv::Mat tt;
    int verticalSize = bw.cols / 40;

    // Create structure element for extracting horizontal lines through morphology operations
    cv::Mat horizontalStructure = cv::getStructuringElement(CV_SHAPE_RECT, cv::Size(1, verticalSize));

    // Apply morphology operations
    erode(bw, tt, horizontalStructure, cv::Point(-1, -1));
    dilate(tt, tt, horizontalStructure, cv::Point(-1, -1));
    //cv::imshow("after erode/dilate", tt);

    //subtract the vertical lines so they won't mess with our text area
    cv::Mat diff;
    cv::subtract(bw, tt, diff);
    //cv::imshow("diff", diff);

    // connect horizontally oriented regions
    cv::Mat connected;
    morphKernel = cv::getStructuringElement(CV_SHAPE_RECT, cv::Size(19, 1));
    morphologyEx(diff, connected, CV_MOP_CLOSE, morphKernel);
    //cv::imshow("connected", connected);

    // find contours
    cv::Mat mask = cv::Mat::zeros(bw.size(), CV_8UC1);
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    findContours(connected, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    std::vector<cv::Rect> rectangles;
    double r = 0;
    // filter contours
    for (int idx = 0; idx >= 0; idx = hierarchy[idx][0]) {
        cv::Rect rect = boundingRect(contours[idx]);
        r = rect.height ? ((double) rect.width / rect.height) : 0;

        //std::cout << "[" << idx << "] r: " << r << "\t\t" << rect.height << "\n";
        if (r > 2 && rect.height > 10) {
            rectangle(rgb, rect, cv::Scalar(0, 0, 255), 1);
            rectangles.push_back(rect);
        }
    }

    //cv::imshow("fields", rgb);
    return rectangles;
}

void Preprocess::find_and_extract_face(cv::Mat &input, cv::Mat &result, cv::Mat &face) {
    cv::CascadeClassifier face_cascade;
    if (!face_cascade.load("/home/ialex/CLionProjects/carditionnative/haarcascades/haarcascade_frontalface_alt.xml")) {
        std::cerr << "Error Loading XML file" << std::endl;
        return;
    }

    // Detect faces
    std::vector<cv::Rect> faces;
    face_cascade.detectMultiScale( input, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, cv::Size(30, 30) );

    if (faces.empty()) {
        std::cerr << "No face found :(" << std::endl;
        return;
    }

    // Filter out the unneeded faces that are in unreasonable places
    auto res = std::remove_if(faces.begin(), faces.end(), [input](cv::Rect face) {
        return face.x > input.cols/3 || face.y > input.rows/3;
    });

    faces.erase(res, faces.end());

    cv::Rect selected_face = faces[0];

    //cv::Point center((int) (face.x + face.width*0.5), (int) (face.y + face.height*0.5 ));
    //cv::ellipse( input, center, cv::Size(face.width*0.5, face.height*0.5), 0, 0, 360, cv::Scalar( 255, 0, 255 ), 4, 8, 0 );

    cv::Rect outline = cv::Rect((int) (selected_face.x * 0.2f), (int) (selected_face.y * 0.65f), (int) (selected_face.width * 1.9f), (int) (selected_face.height * 1.9f));
    //cv::rectangle(input, outline,  cv::Scalar( 0, 0, 255 ), 3);

    face = input(outline);

    input.copyTo(result);
    cv::Mat mask = cv::Mat(outline.width, outline.height, input.type(), cv::Scalar(234, 232, 227));
    mask.copyTo(result(outline));
}

//
// Created by ialex on 22.09.2018.
//

#ifndef PERSPECTIVEWARP_OCR_H
#define PERSPECTIVEWARP_OCR_H

#include <tesseract/baseapi.h>

#include <opencv2/core.hpp>
#include "RomanianID.h"

class OCR {
public:
    ~OCR();

    void test_tess(cv::Mat&, std::vector<cv::Rect>, RomanianID&);
    void parse(std::string&, RomanianID&);

private:
    bool first = true;
    void init();
};


#endif //PERSPECTIVEWARP_OCR_H

#include <iostream>
#include <fstream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/dnn.hpp>
#include <opencv/cv.hpp>

#include <json.hpp>
#include <iomanip>

#include "Preprocess.h"
#include "Utils.h"
#include "OCR.h"
#include "DocumentFiles.h"

Preprocess pre;
Utils utils;
OCR ocr;

void build_doc(const std::string &str, document_t &doc) {
    //std::cout << "Splitting: " << str << '\n';
    std::size_t found = str.find_last_of("/\\");
    std::string path = str.substr(0, found);
    std::string jpeg = str.substr(found + 1, str.length());

    std::size_t found1 = jpeg.find_last_of(".\\");
    std::string filename = jpeg.substr(0, found1);
    std::string ext = jpeg.substr(found1 + 1, jpeg.length());

    std::string json = path + "/" + filename + ".json";
    std::string photo = path + "/" + filename + "_face." + ext;

    doc.original_file = str;
    doc.json_file = json;
    doc.face_photo_file = photo;
}

void write_json(const document_t &doc, const nlohmann::json &js) {
    std::ofstream json_file(doc.json_file);
    if (!json_file.is_open()) {
        return;
    }

    json_file << std::setw(4) << js << std::endl;
    json_file.close();

    std::cout << doc.json_file;
}

int main(int argc, char **argv) {
    document_t document; build_doc(argv[1], document);
    nlohmann::json js = nlohmann::json();

    cv::Mat flat_object = cv::imread(document.original_file);
    utils.scale(flat_object);

    cv::Mat deskew;

    pre.deskew(flat_object, deskew);
    //cv::imshow("result", deskew);

    cv::Mat without_face;
    cv::Mat face;
    pre.find_and_extract_face(deskew, without_face, face);
    //cv::imshow("face", face);
    //cv::imshow("without_face", without_face);

    std::vector<cv::Rect> fields = pre.identify_fields(without_face);
    if (fields.empty()) {
        js["status"] = "error";
        write_json(document, js);

        std::cerr << "No fields detected\n";
        return -1;
    }

    RomanianID id_card;
    ocr.test_tess(deskew, fields, id_card);

    js["status"] = "success";

    js["file"]["original_file"] = document.original_file;
    js["file"]["json_file"] = document.json_file;
    js["file"]["face_file"] = document.face_photo_file;

    js["fields"]["mrz"] = id_card.mrz_fields;
    js["fields"]["other"] = id_card.other_fields;

    //std::cout << std::setw(4) << js << std::endl;

    write_json(document, js);
    cv::imwrite(document.face_photo_file, face);

    return 0;
}
//
// Created by ialex on 22.09.2018.
//

#ifndef PERSPECTIVEWARP_PREPROCESS_H
#define PERSPECTIVEWARP_PREPROCESS_H

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/dnn.hpp>
#include <opencv/cv.hpp>

class identify_fields;

class Preprocess {
public:
    void deskew(cv::Mat&, cv::Mat&);

    void find_and_extract_face(cv::Mat&, cv::Mat&, cv::Mat &result);

    std::vector<cv::Rect> identify_fields(cv::Mat &rgb);

private:

    void four_point_warp(std::vector<cv::Point> &contour, cv::Mat &orig, cv::Mat &warp);
};


#endif //PERSPECTIVEWARP_PREPROCESS_H

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/sergiu/Documents/playground/megahack/document-parser/OCR.cpp" "/home/sergiu/Documents/playground/megahack/document-parser/build/CMakeFiles/PerspectiveWarp.dir/OCR.cpp.o"
  "/home/sergiu/Documents/playground/megahack/document-parser/Preprocess.cpp" "/home/sergiu/Documents/playground/megahack/document-parser/build/CMakeFiles/PerspectiveWarp.dir/Preprocess.cpp.o"
  "/home/sergiu/Documents/playground/megahack/document-parser/RomanianID.cpp" "/home/sergiu/Documents/playground/megahack/document-parser/build/CMakeFiles/PerspectiveWarp.dir/RomanianID.cpp.o"
  "/home/sergiu/Documents/playground/megahack/document-parser/Utils.cpp" "/home/sergiu/Documents/playground/megahack/document-parser/build/CMakeFiles/PerspectiveWarp.dir/Utils.cpp.o"
  "/home/sergiu/Documents/playground/megahack/document-parser/main.cpp" "/home/sergiu/Documents/playground/megahack/document-parser/build/CMakeFiles/PerspectiveWarp.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

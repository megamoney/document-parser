//
// Created by ialex on 22.09.2018.
//

#ifndef PERSPECTIVEWARP_DOCUMENTFILES_H
#define PERSPECTIVEWARP_DOCUMENTFILES_H

#include <string>

struct document_t {
    std::string original_file;
    std::string json_file;
    std::string face_photo_file;
};

#endif //PERSPECTIVEWARP_DOCUMENTFILES_H
